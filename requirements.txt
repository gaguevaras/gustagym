Django==2.1.5
djangorestframework==3.9.1
djangorestframework-simplejwt==3.3
freezegun==0.3.11
PyJWT==1.7.1
python-dateutil==2.7.5
pytz==2018.9
six==1.12.0
