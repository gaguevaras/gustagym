from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from workouts.views import UserViewSet, PlanViewSet, DayViewSet, ExerciseViewSet, assignment_list, assign

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'plans', PlanViewSet)
router.register(r'days', DayViewSet)
router.register(r'exercises', ExerciseViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^assign/(?P<plan_id>\d+)/$', assign),
    url(r'^assign/list/(?P<plan_id>\d+)/$', assignment_list),

]
