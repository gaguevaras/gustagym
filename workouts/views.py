from django.contrib.auth.models import User
from django.core.mail import send_mail
from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from workouts.models import WorkoutPlan, WorkoutDay, Exercise
from workouts.serializers import UserSerializer, PlanSerializer, DaySerializer, ExerciseSerializer
from workouts.util import notify_users


class UserViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


class PlanViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = WorkoutPlan.objects.all()
    serializer_class = PlanSerializer


class DayViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = WorkoutDay.objects.all()
    serializer_class = DaySerializer


class ExerciseViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer


@api_view(['GET'])
def assignment_list(request, plan_id):
    """
    List all code snippets, or create a new snippet.
    """
    try:
        plan = WorkoutPlan.objects.get(pk=plan_id)
    except:
        return Response({}, status=status.HTTP_400_BAD_REQUEST)

    return Response(plan.users, status=status.HTTP_200_OK)


@api_view(['POST'])
def assign(request, plan_id):
    """
    List all code snippets, or create a new snippet.
    """
    try:
        plan = WorkoutPlan.objects.get(pk=plan_id)
        users = request.data['users']
        plan.users.set(users)
        notify_users(users, plan)

    except:
        return Response({"message": "Assignment Failed"}, status=status.HTTP_400_BAD_REQUEST)

    return Response({}, status=status.HTTP_200_OK)
