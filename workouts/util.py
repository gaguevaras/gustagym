from django.contrib.auth.models import User
from django.core.mail import send_mail


def notify_users(users, plan):
    for user in users:
        send_mail(
            'You have been assigned to plan {}'.format(plan.name),
            'Congrats!',
            'gustavo@virtuagym.com',
            [User.objects.get(pk=user).email],
            fail_silently=False,
        )
