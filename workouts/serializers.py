from django.contrib.auth.models import User
from rest_framework import serializers

from workouts.models import WorkoutPlan, WorkoutDay, Exercise
from workouts.util import notify_users


class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=False)

    class Meta:
        model = User
        fields = '__all__'


class ExerciseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Exercise
        fields = ('name',)
        extra_kwargs = {
            'name': {'validators': []},
        }

    def create(self, validated_data):
        return super(ExerciseSerializer, self).create(validated_data)

    def update(self, validated_data, **kwargs):
        return super(ExerciseSerializer, self).update(self, validated_data)


class DaySerializer(serializers.ModelSerializer):
    exercises = ExerciseSerializer(many=True, required=False)

    class Meta:
        model = WorkoutDay
        fields = ('id', 'day_number', 'exercises')

    def create(self, validated_data):
        exercises_data = validated_data.pop('exercises', [])
        day = WorkoutDay.objects.create(**validated_data)
        for exercise_data in exercises_data:
            Exercise.objects.create(day=day, **exercise_data)
        return day

    def update(self, validated_data, **kwargs):
        return super(DaySerializer, self).update(self, validated_data)


class PlanSerializer(serializers.ModelSerializer):
    users = UserSerializer(many=True, read_only=True)
    days = DaySerializer(many=True, required=False)
    name = serializers.CharField(required=False)

    class Meta:
        model = WorkoutPlan
        fields = ('id', 'name', 'days', 'users')
        extra_kwargs = {
            'users': {'validators': []},
        }

    def create(self, validated_data):
        days_data = validated_data.pop('days', [])
        plan = WorkoutPlan.objects.create(**validated_data)
        for day_data in days_data:
            exercises_data = day_data.pop('exercises', [])
            day = WorkoutDay.objects.create(plan=plan, **day_data)
            for exercise_data in exercises_data:
                exercise = Exercise.objects.get_or_create(**exercise_data)[0] # returns (obj, is_created)
                exercise.days.add(day)
        return plan

    def update(self, plan, validated_data):
        users = validated_data.pop('users', [])
        days_data = validated_data.pop('days', [])
        plan.name = validated_data.pop('name', plan.name)
        notify_users(users, plan)
        plan.users.set(users)
        WorkoutDay.objects.filter(plan=plan).delete()
        for day_data in days_data:
            exercises_data = day_data.pop('exercises', [])
            day = WorkoutDay.objects.update_or_create(plan=plan, **day_data)[0]
            day.exercises.set([])
            for exercise_data in exercises_data:
                exercise = Exercise.objects.update_or_create(**exercise_data)[0]
                exercise.days.add(day)
        plan.save()
        return plan
