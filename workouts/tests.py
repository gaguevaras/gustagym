import datetime

from django.core import mail
from django.test import TestCase
from freezegun import freeze_time
from rest_framework import status
from rest_framework.test import APIClient

from workouts.models import WorkoutPlan


class TokenTests(TestCase):

    fixtures = ['initial_data']

    def setUp(self):
        self.client = APIClient()
        body = {
            "username": "gaguevaras",
            "password": "hireme"
        }
        response = self.client.post('/api/token/', body, format='json')
        self.refresh_token = response.data['refresh']
        self.access_token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + response.data['access'])

    def test_request_token(self):
        """
            Request a token using basic authentication.
        """
        body = {
            "username": "gaguevaras",
            "password": "hireme"
        }

        response = self.client.post('/api/token/', body, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('refresh', response.data)
        self.assertIn('access', response.data)
        self.assertIsNotNone(response.data['access'])
        self.assertIsNotNone(response.data['refresh'])

    def test_refresh_token(self):
        """
            Refresh the access token.
        """
        body = {
            "refresh": self.refresh_token
        }

        response = self.client.post('/api/token/refresh/', body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('access', response.data)
        self.assertIsNotNone(response.data['access'])

    def test_refresh_token_fails_after_5_min_from_expiration(self):

        refresh_client = APIClient()

        initial_datetime = datetime.datetime(year=1, month=7, day=12, hour=15, minute=0, second=3)
        too_late_datetime = datetime.datetime(year=1, month=7, day=12, hour=15, minute=11, second=3)

        with freeze_time(initial_datetime) as frozen_datetime:
            assert frozen_datetime() == initial_datetime
            response = refresh_client.post('/api/token/', {'username': 'gaguevaras', 'password': 'hireme'}, format='json')
            refresh_token = response.data['refresh']

            frozen_datetime.move_to(too_late_datetime)
            assert frozen_datetime() == too_late_datetime

            refresh_client.credentials(HTTP_AUTHORIZATION='Token ' + response.data['access'])
            response = refresh_client.get('/workouts/users/', format='json')
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

            response = refresh_client.post('/api/token/refresh/', {'refresh': refresh_token}, format='json')
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class WorkoutTests(TestCase):

    fixtures = ['initial_data']

    def setUp(self):
        self.client = APIClient()
        body = {
            "username": "gaguevaras",
            "password": "hireme"
        }
        response = self.client.post('/api/token/', body, format='json')
        self.refresh_token = response.data['refresh']
        self.access_token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.access_token)

    def test_create_plan(self):
        body = {
            'name': 'Cardio',
            'days': [
                {
                    'day_number': 1,
                    'exercises': [
                        {
                            'name': 'Chest'
                        },
                        {
                            'name': 'Back'
                        }
                    ]

                },
                {
                    'day_number': 2,
                    'exercises': [
                        {
                            'name': 'Leg'
                        },
                        {
                             'name': 'Calves'
                        }
                    ]
                }
            ]
        }

        self.assertEqual(WorkoutPlan.objects.filter(name='Cardio').count(), 0)
        response = self.client.post('/workouts/plans/', body, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(WorkoutPlan.objects.filter(name='Cardio').count(), 1)

    def test_load_plan(self):
        body = {
            'name': 'Cardio',
            'days': []
        }
        self.client.post('/workouts/plans/', body, format='json')
        response = self.client.get('/workouts/plans/', format='json')
        self.assertEqual(len(response.data), 1)

    def test_update_plan(self):
        body = {
            'name': 'Cardio',
            'days': []
        }
        plan_id = self.client.post('/workouts/plans/', body, format='json').data['id']
        updated_body = {
            'name': 'Cardio Renamed',
            'days': [{
                'day_number': 1,
                'exercises': [
                    {
                        'name': 'Run'
                    },
                    {
                        'name': 'Swim'
                    }
                ]

            }]
        }
        response = self.client.put('/workouts/plans/{}/'.format(plan_id), updated_body, format='json')
        self.assertEqual(WorkoutPlan.objects.get(id=plan_id).name, 'Cardio Renamed')

    def test_delete_plan(self):
        body = {
            'name': 'Cardio',
            'days': []
        }
        creation_response = self.client.post('/workouts/plans/', body, format='json')
        response = self.client.delete('/workouts/plans/{}/'.format(creation_response.data['id']), format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get('/workouts/plans/', format='json')
        self.assertEqual(len(response.data), 0)

    def test_plan_assignment(self):

        body = {
            'name': 'Cardio',
            'days': []
        }
        plan_id = self.client.post('/workouts/plans/', body, format='json').data['id']
        updated_body = {
            'users': [1]
        }
        response = self.client.post('/workouts/assign/{}/'.format(plan_id), updated_body, format='json')
        self.assertIn(1, WorkoutPlan.objects.get(id=plan_id).users.values_list('id', flat=True))
        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        # Verify that the subject of the first message is correct.
        self.assertEqual(mail.outbox[0].subject, 'You have been assigned to plan Cardio')
