from django.contrib.auth.models import User
from django.db import models
from django.db.models import CASCADE


class WorkoutPlan(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    users = models.ManyToManyField(User)


class WorkoutDay(models.Model):

    id = models.AutoField(primary_key=True)
    day_number = models.IntegerField()
    plan = models.ForeignKey(WorkoutPlan, related_name='days', on_delete=CASCADE)

    class Meta:
        unique_together = ('day_number', 'plan')
        ordering = ['day_number']


class Exercise(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    days = models.ManyToManyField(WorkoutDay, related_name='exercises')
