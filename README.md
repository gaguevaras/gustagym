Instructions:

1.  Clone the repository: 

`git clone https://gitlab.com/gaguevaras/gustagym.git`

2.  Create a Python 3.7 Virtual Environment:

`virtualenv venv` 

3.  Activate the virtualenv:

`source venv/bin/activate`

4.  Install the project requirements:

`pip install -r requirements.txt`


5.  Execute the test suite:

`./manage.py test`


6.  Read the code! 